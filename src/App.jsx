import { useState } from "react";

import "./App.css";
import Form from "./component/Form/Form";
import Tasks from "./component/Tasks/Tasks";
import Header from "./component/Header/Header";

function App() {
  const [tasks, setTasks] = useState([
    {
      task: "Drink two glass of water daily",
      date: "2024-01-16",
      status: "Not complete",
    },
    {
      task: "Read two books",
      date: "2024-01-16",
      status: "Not complete",
    },
  ]);

  return (
    <>
      <div className="main">
        <Header  />
        <Form  tasks setTasks={setTasks} />
        <Tasks tasks={tasks} setTasks={setTasks} />
      </div>
    </>
  );
}

export default App;
