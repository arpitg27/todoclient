import React from "react";

import "./Tasks.css"
const Tasks = ({ tasks, setTasks }) => {
  const handleStatusChange = (index) => {
    
    const newTasks = [...tasks];
    newTasks[index] = {
      ...newTasks[index],
      status: "Completed",
    };
    setTasks(newTasks);
  };
    
  const handleDelete = (index) => {
    const newTasks = [...tasks];
   const filteredTasks=newTasks.filter((task,ind) =>{
         return !(ind===index)
   })
      
    setTasks(filteredTasks);
  };


  return (
    <div style={{marginTop:"20px"}} className="mainContent">
      {tasks.map((element, index) => {
        return (
         
          <div className="task" key={index}>
            <div className="task-string" >{element.task}</div>

            <div className="task-date"> {element.date}</div>
            <div  className="task-status">{element.status}</div>

          { (element.status==="Not complete")&&<button
              className="button"
              onClick={() => {
                handleStatusChange(index);
              }}
              
            >
              Mark as Done
            </button>} 
            <button
              className="button"
              style={{backgroundColor:'red'}}
              onClick={() => {
                handleDelete(index);
              }}
            >
              Delete
            </button>
            </div>
           
        );
      })}
    </div>
  );
};

export default Tasks;
