import React, { useState } from "react";

import "./Form.css";

export default function Form({ tasks, setTasks }) {
  const [task, setTask] = useState({
    task: "",
    date: "",
    status: "Not complete",
  });

  const handleSubmit = (event) => {
    event.preventDefault();
    setTasks((prev) => [...prev, task]);
    setTask({ task: "", date: "",status:"Not complete" });
  };
  const handleChange = (event) => {
    const { name, value } = event.target;
    setTask((task) => ({
      ...task,
      [name]: value,
    }));
  };

  return (
    <form style={{borderBottom:"2px solid #2980b9"}} className="mainContent" onSubmit={handleSubmit}>
      <label className="input-container">
        <input
          className="formInput  input-field"
          name="task"
          type="text"
          placeholder="Enter the task"
          required
          value={task.task}
          onChange={handleChange}
        />
      </label>
      <label  className="input-container">
        <input
          className="formInput input-field"
          name="date"
          type="date"
          required
          value={task.date}
          onChange={handleChange}
        />
      </label>

      <button style={{ backgroundColor: "" ,marginBottom:"20px"}} className="button" type="submit">
        Submit
      </button>
    </form>
  );
}
